#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include <cstdio>

namespace program
{
    inline void foo()
    {
        printf("Hello World!\n");
    }
}

#endif
