;ELC   
;;; Compiled
;;; in Emacs version 27.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(mapc 'require '(kproperty kmenu kview))
#@69 Version number of persistent data format used for saving koutlines.
(defconst kfile:version "Kotl-4.0" (#$ . 449))
(add-hook 'after-save-hook 'kfile:narrow-to-kcells)
#@76 Value of `print-escape-newlines' used by `kfile:print-to-string' function.
(defvar kfile:escape-newlines t (#$ . 622))
#@93 Find a file FILE-NAME containing a kotl or create one if none exists.
Return the new kview.
(defalias 'kfile:find #[(file-name) "\305!\306\211\203 \307!\204 \310\311\"\210\312!\313!\204$ \314	\n\"\210\f\315=\204- \315 \210*\207" [file-name buffer existing-file kview major-mode file-exists-p nil file-readable-p error "(kfile:find): \"%s\" is not readable.  Check permissions" find-file kview:is-p kfile:read kotl-mode] 4 (#$ . 747) (list (kfile:read-name "Find koutline file: " nil))])
#@112 Iff current buffer contains an unformatted or formatted koutline, return file format version string, else nil.
(defalias 'kfile:is-p #[nil "\301\212\214~\210eb\210\3021\" \303p!\211;\205 \304\305\"\205 0\202$ \210\301+\207" [ver-string nil (error) read string-match "^Kotl-"] 4 (#$ . 1252)])
#@69 View an existing kotl version-2 file FILE-NAME in a read-only mode.
(defalias 'kfile:view #[(file-name) "\302!\211\203 \303!\204 \304\305\"\210\202 \304\306\"\210\307!\210)\310 \210eb\207" [file-name existing-file file-exists-p file-readable-p error "(kfile:view): \"%s\" is not readable.  Check permissions" "(kfile:view): \"%s\" does not exist" view-file kfile:narrow-to-kcells] 4 (#$ . 1556) (list (kfile:read-name "View koutline file: " t))])
#@108 Create a new koutline file attached to BUFFER, with a single empty level 1 kotl cell.
Return file's kview.
(defalias 'kfile:create #[(buffer) "\204 p\306!\204 \307\310\"\210q\210	\203 \307\311\"\210~\210\312 \313U\314\211\211\211\2048 \315!q\210\316 \210\317\320!!peb\210\321\322!\210\323 !\210\321\324!\210db\210\321\325!\210\321\326!\210\327 \210eb\210\203x \330\331\332\"\210\333\314!\210eb\210\334 b\210\202\217 ed|\210\335\fp\"\210\320\f!\313H\336=\203\217 \337\f!\210,\207" [buffer buffer-read-only standard-output view import-from empty-p bufferp error "(kfile:create): Invalid buffer argument, %s" "(kfile:create): %s is read-only" buffer-size 0 nil kimport:copy-and-set-buffer erase-buffer kview:create buffer-name princ ";; -*- Mode: kotl -*- \n" prin1 " ;; file-format\n\n" "\n\n\n" "\n;; depth-first kcell attributes\n" kfile:narrow-to-kcells kview:add-cell "1" 1 set-buffer-modified-p kcell-view:start kimport:file 32 kill-buffer kfile:version] 5 (#$ . 2019)])
#@239 Create a new kotl view by reading BUFFER or create an empty view when EXISTING-FILE-P is nil.
Optional VER-STRING is the outline format version number for the BUFFER that
was previously read by calling `kfile:is-p'.

Return the new view.
(defalias 'kfile:read #[(buffer existing-file-p &optional ver-string) "\303!\204 \304\305\"\207\306 \307U\203 	\204 \310!\207q\210\n;\204. \311 \211\204. \304\312\"\207\n\313\267\202F \314\315\"\207\314\316\"\207\317!\207\304\320!\207\304\321\n#\207" [buffer existing-file-p ver-string bufferp error "(kfile:read): Argument must be a buffer, `%s'" buffer-size 0 kfile:create kfile:is-p "(kfile:read): `%s' is not a koutline file" #s(hash-table size 4 test equal rehash-size 1.5 rehash-threshold 0.8125 purecopy t data ("Kotl-4.0" 52 "Kotl-3.0" 57 "Kotl-2.0" 62 "Kotl-1.0" 66)) kfile:read-v4-or-v3 nil t kfile:read-v2 "(kfile:read): V1 koutlines are no longer supported" "(kfile:read): `%s' has unknown kotl version, %s"] 4 (#$ . 3025)])
#@76 Create a kotl view by reading kotl version-2 BUFFER.  Return the new view.
(defalias 'kfile:read-v2 #[(buffer) "\306\211\211\211\211\211\211\211\211~\210eb\210\307\310\306\311\312$\210\313 \313 \313 \313 \313 \313 \313 \314\f\"\315\316!&\317 \210eb\210\320\n	\"\210\321\306!\210eb\210\322 b\210\n.\n\207" [buffer kcell-list view kotl-structure cell-data level-indent nil search-forward "\n" t 2 read kfile:build-structure-v2 kview:create buffer-name kfile:narrow-to-kcells kfile:insert-attributes-v2 set-buffer-modified-p kcell-view:start label-separator label-min-width label-type cell-count standard-input] 10 (#$ . 4019)])
#@123 Create a koutline view by reading version-4 BUFFER.  Return the new view.
If V3-FLAG is true, read as a version-3 buffer.
(defalias 'kfile:read-v4-or-v3 #[(buffer v3-flag) "\306\211\211\211\211\211\211~\210eb\210\307\310\306\311\312$\210\204+ \313 \210\314 \314 \314 \314 \314 \314 \314 \315\316!\f&\317 \210eb\210\320	\n\"\210\321\306!\210eb\210\322 b\210	.\207" [buffer view cell-data level-indent label-separator label-min-width nil search-forward "\n" t 2 kvspec:initialize read kview:create buffer-name kfile:narrow-to-kcells kfile:insert-attributes-v3 set-buffer-modified-p kcell-view:start label-type cell-count standard-input v3-flag kvspec:current] 8 (#$ . 4694)])
#@228 Update kfile internal structure so that view is ready for saving to a file.
Leave outline file expanded with structure data showing unless optional
VISIBLE-ONLY-P is non-nil.  Signal an error if kotl is not attached to a file.
(defalias 'kfile:update #[(&optional visible-only-p) "\306!\307	\302\"\310!\311!\312!\313!'\314(\n\204( \315\316!\210\2023 \317\n!\2043 \315\320\n\"\210\314)\307	\321\"\322T\314\"*p+\323 `\314\223,\324-\314.\325 \210\326\327\330#\210*\331\332	!I\210\307	\321\"~\210eb\210\333\334\314\330#\203} e\331\225|\210\335\336!\210\337/!\210\335\340!\210\333\334\314\330#\203\241 \331\224b\210\341\314x\210`d|\210\202\244 db\210\335\342!\210\335\343\3440\f'&!\210\335\345!\210\346*!\2101\203\311 \325 \210,b\210,\314\211\223\210.\314\207" [kview top file label-type label-min-width label-separator kview:top-cell kcell:get-attr kview:label-type kview:label-min-width kview:label-separator kview:level-indent nil error "(kfile:update): Current outline is not attached to a file" file-writable-p "(kfile:update): File \"%s\" is not writable" id-counter make-vector make-marker 1 kfile:narrow-to-kcells kview:map-tree #[(view) "\303 	\n\304!I\210\nT\211\207" [cell kcell-data kcell-num kcell-view:cell kcell-data:create] 4] t 0 kcell-data:create search-forward "\n\n" princ ";; -*- Mode: kotl -*- \n" prin1 " ;; file-format\n\n" "\n" "\n\n\n" format "%S ;; kvspec:current\n%d ;; id-counter\n%S ;; label-type\n%d ;; label-min-width\n%S ;; label-separator\n%d ;; level-indent\n" "\n;; depth-first kcell attributes\n" kfile:pretty-print level-indent debug-on-error buffer-read-only kcell-data standard-output opoint kcell-num cell kfile:version kvspec:current visible-only-p] 9 (#$ . 5410)])
#@32 Write current outline to FILE.
(defalias 'kfile:write #[(file) "\203\n \303\230\203 \304\305\"\210\306!\203! 	\203! \307!\310	!P\311\312\n!\300#\210\313!\210\314 \210\315\316!\210\317\np\"\210\320 \207" [file buffer-file-name kview "" error "(kfile:write): Invalid file name, \"%s\"" file-directory-p file-name-as-directory file-name-nondirectory kcell:set-attr kview:top-cell set-visited-file-name kotl-mode set-buffer-modified-p t kview:set-buffer save-buffer] 4 (#$ . 7165) "FWrite outline file: "])
#@259 Build cell list from the KOTL-STRUCTURE and its CELL-DATA.
Assumes all arguments are valid.  CELL-DATA is a vector of cell fields read
from a koutline file.

Return list of outline cells in depth first order.  Invisible top cell is not
included in the list.
(defalias 'kfile:build-structure-v2 #[(kotl-structure cell-data) "\306\211\211\211\211\307\310\"\210\n\237-\207" [cell func cell-list sibling-p stack kotl-structure nil mapc #[(item) "\306\307\310B\311\312BD\"A\211\203 	 \207\313\232\203 \314\207\315\nH!\211\fB\316\211\207" [item func cell-data cell cell-list sibling-p assoc "(" #[nil "	B\302\211\207" [sibling-p stack nil] 2] ")" #[nil "@A\211\207" [stack sibling-p] 2] 0 nil kcell-data:to-kcell-v2 t] 6]] 5 (#$ . 7684)])
#@143 Set cell attributes within KVIEW for each element in KCELL-LIST.
Assumes all cell contents are already in kview and that no cells are
hidden.
(defalias 'kfile:insert-attributes-v2 #[(kview kcell-list) "\302\303\302w\210\304\305\302\306#\203 \307\310	@\"\210	A\311\312\302\306#\205\" \202 )\207" [buffer-read-only kcell-list nil "\n" re-search-forward "[A-Za-z0-9]\\(\\.?[A-Za-z0-9]\\)*" t kproperty:set kcell search-forward "\n\n"] 4 (#$ . 8446)])
#@145 Set cell attributes within KVIEW for each element in KCELL-VECTOR.
Assumes all cell contents are already in kview and that no cells are
hidden.
(defalias 'kfile:insert-attributes-v3 #[(kview kcell-vector) "\303\304\305\304w\210\306\307\304\310#\203 \311\312\313\n	H!\"\210	T\314\315\304\310#\205' \202 *\207" [buffer-read-only kcell-num kcell-vector 1 nil "\n" re-search-forward "[A-Za-z0-9]\\(\\.?[A-Za-z0-9]\\)*" t kproperty:set kcell kcell-data:to-kcell-v3 search-forward "\n\n"] 5 (#$ . 8905)])
#@41 Narrow kotl file to kcell section only.
(defalias 'kfile:narrow-to-kcells #[nil "\303!\205A \304\211\212~\210eb\210\305\306\304\307#\203 \310\225T\n\203, \305\306\304\307#\203, \310\224T\n\203= 	\203= \n	}\210eb\202@ \311\312!+\207" [kview end-text start-text kview:is-p nil search-forward "\n" t 0 error "(kfile:narrow-to-kcells): Cannot find start or end of kcells"] 4 (#$ . 9415) nil])
#@183 Return a string containing OBJECT, any Lisp object, in pretty-printed form.
Quoting characters are used when needed to make output that `read' can
handle, whenever this is possible.
(defalias 'kfile:print-to-string #[(object) "r\305\306!q\210\307\211\310 \210\311\216\312 \210\n\313\fp\"\210)eb\210m\204\362 \314\315!\2033 \314\315!\203 \316u\210\202' \314\317!\203\201 \316\224\316V\203\201 \316\224Sf\320=\203\201 \212\321\224b\210\322 \210`f)\323=\203\201 \316\224S\316\225|\210\324c\210\322\316!\210\314\325!\203v \326\224\326\225|\210\202z \327\330!\210\331\316!\210\202 \3321\216 \333\334\316!\2100\202\222 \210\202\262 \203\262 \335u\210\336\307x\210`\336\307w\210`|\210`Sf\337=\204 \340c\210\202 \3411\277 \333\342\316!\2100\202\303 \210\202\354 \203\354 \314\343!\203\322 \316u\210\202\306 \336\307x\210`\336\307w\210`|\210`Sf\337=\204 \340c\210\202 db\210\202 eb\210\344 \210\345 ,\207" [buffer-read-only emacs-lisp-mode-hook kfile:escape-newlines print-escape-newlines object get-buffer-create " kfile:print-to-string" nil erase-buffer #[nil "\300p!\207" [kill-buffer] 2] emacs-lisp-mode prin1 looking-at "\\s(" 1 "\\(quote[ 	]+\\)\\([^.)]\\)" 40 2 forward-sexp 41 "'" "[ 	]*)" 0 error "Malformed quote" backward-sexp (error) t down-list -1 " 	" 39 10 (error) up-list "\\s)" indent-sexp buffer-string] 3 (#$ . 9818)])
#@247 Output the pretty-printed representation of OBJECT, any Lisp object.
Quoting characters are printed when needed to make output that `read'
can handle, whenever this is possible.
Output stream is STREAM, or value of `standard-output' (which see).
(defalias 'kfile:pretty-print #[(object &optional stream) "\303\304!	\206	 \n\"\207" [object stream standard-output princ kfile:print-to-string] 3 (#$ . 11164)])
#@73 PROMPT for and read a koutline file name.  EXISTING-P means must exist.
(defalias 'kfile:read-name #[(prompt existing-p) "\303\204  \304	\303\211\n$\211\203 \305\232\203 \306 \210\303\211\203 )\207" [filename prompt existing-p nil read-file-name "" beep] 6 (#$ . 11579)])
(provide 'kfile)
